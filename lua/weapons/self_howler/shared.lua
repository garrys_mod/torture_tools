SWEP.PrintName = "Self howler"
SWEP.Author = "(Pyth0n11)"
SWEP.Purpose = "Left click = Short scream \nRight click = Long scream \n Reload = change the gender of the screamer."

SWEP.Spawnable = true
SWEP.ViewModel = ""
SWEP.WorldModel = ""
SWEP.Category = "Torture tools"

SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1

SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1

SWEP.DrawCrosshair = false
SWEP.DrawAmmo = false

SWEP.screamer_gender = "man"
SWEP.next_reload_time = 3
SWEP.last_reload_time = 0

local woman_short_scream = {"torture_tools/woman_scream_short_1.wav", "torture_tools/woman_scream_short_2.wav", "torture_tools/woman_scream_short_3.wav"}
local man_short_scream = {"torture_tools/man_scream_short_1.wav", "torture_tools/man_scream_short_2.wav", "torture_tools/man_scream_short_3.wav"}
local woman_long_scream = {"torture_tools/woman_scream_long_1.wav", "torture_tools/woman_scream_long_2.wav"}
local man_long_scream = {"torture_tools/man_scream_long_1.wav", "torture_tools/man_scream_long_2.wav"}

function SWEP:Initialize()
    self:SetHoldType("normal")
end

function SWEP:PrimaryAttack ()
    if not IsFirstTimePredicted() then return end -- If it not checking first time predicted, it will be done many times. 
    -- Emit random sound for scream
    if SERVER then
        if self.screamer_gender == "man" then
            self.Owner:EmitSound(man_short_scream[math.random(1, #man_short_scream)])
        else
            self.Owner:EmitSound(woman_short_scream[math.random(1, #woman_short_scream)])
        end
    end

    self.Weapon:SetNextPrimaryFire(CurTime() + 5)
end

function SWEP:SecondaryAttack()
    if not IsFirstTimePredicted() then return end -- If it not checking first time predicted, it will be done many times. 
    -- Emit random sound for scream.
    if SERVER then
        if self.screamer_gender == "man" then
            self.Owner:EmitSound(man_long_scream[math.random(1, #man_long_scream)])
        else
            self.Owner:EmitSound(woman_long_scream[math.random(1, #woman_long_scream)])
        end
    end

    self.Weapon:SetNextSecondaryFire(CurTime() + 15)
end

function SWEP:Reload()
    if (game.SinglePlayer()) then self:CallOnClient("Reload") end -- Force the client to read Reload function in singleplayer.
    if not IsFirstTimePredicted() then return end -- If it not checking first time predicted, it will be done many times.
    if CurTime() < self.last_reload_time + self.next_reload_time then return end
    
    self.last_reload_time = CurTime()
    if CLIENT then
        if self.screamer_gender == "man" then
            self.screamer_gender = "woman"
            LocalPlayer():ChatPrint("The screamer is now a woman")
        else
            self.screamer_gender = "man"
            LocalPlayer():ChatPrint("The screamer is now a man")
        end
        net.Start("update_vendemiaire_selfhowler_config")
        net.WriteBool(self.screamer_gender)
        net.SendToServer()
    end
end

-- Change the screamer_gender var in the server side.
if SERVER then
    local function update_vendemiaire_selfhowler_config(ln, ply)
        ply_self_howler = ply:GetWeapon("self_howler")
        if ply_self_howler:IsValid() then
            ply_self_howler.screamer_gender = net.ReadBool()
        end
    end
    
    net.Receive("update_vendemiaire_selfhowler_config", update_vendemiaire_selfhowler_config)
end 
