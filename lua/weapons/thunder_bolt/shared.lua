SWEP.PrintName = "Thunderbolt"
SWEP.Author = "(Pyth0n11)"
SWEP.Purpose = "Left click = Short scream \nRight click = Long scream \n Reload = change the gender of the screamer."

SWEP.Spawnable = true
SWEP.ViewModel = "models/weapons/v_stunstick.mdl"
SWEP.WorldModel = "models/weapons/w_stunbaton.mdl"
SWEP.Category = "Torture tools"

SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1

SWEP.Secondary.Automatic = false
SWEP.Secondary.Ammo = "none"
SWEP.Secondary.ClipSize = -1
SWEP.Secondary.DefaultClip = -1

SWEP.DrawCrosshair = false
SWEP.DrawAmmo = false

SWEP.screamer_gender = "man"
SWEP.next_reload_time = 3
SWEP.last_reload_time = 0

local thunderbolt_short = {"torture_tools/thuderbolt_short_lightning_1.wav", "torture_tools/thuderbolt_short_lightning_2.wav", "torture_tools/thuderbolt_short_lightning_3.wav"}
local thunderbolt_long = {"torture_tools/thuderbolt_long_lightning_1.wav", "torture_tools/thuderbolt_long_lightning_2.wav"}
local woman_short_scream = {"torture_tools/woman_scream_short_1.wav", "torture_tools/woman_scream_short_2.wav", "torture_tools/woman_scream_short_3.wav"}
local man_short_scream = {"torture_tools/man_scream_short_1.wav", "torture_tools/man_scream_short_2.wav", "torture_tools/man_scream_short_3.wav"}
local woman_long_scream = {"torture_tools/woman_scream_long_1.wav", "torture_tools/woman_scream_long_2.wav"}
local man_long_scream = {"torture_tools/man_scream_long_1.wav", "torture_tools/man_scream_long_2.wav"}

function SWEP:Initialize()
    self:SetHoldType("knife")
end

function SWEP:PrimaryAttack ()
    if not IsFirstTimePredicted() then return end -- If it not checking first time predicted, it will be done many times. 
    local vstart = self.Owner:EyePos() + Vector(0, 0, -10)
    local trace = util.TraceLine({start=vstart, endpos = vstart + self.Owner:GetAimVector() * 75, filter = self.Owner})
    
    ent = trace.Entity
    if not (ent:IsPlayer() or ent:IsNPC()) then return end

    self.Owner:SetAnimation(PLAYER_ATTACK1)
    self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
    bullet = {}
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Damage = 20
    self.Owner:FireBullets(bullet)
    
    -- Emit random sound for effect and scream.
    if SERVER then
        ent:EmitSound(thunderbolt_short[math.random(1, #thunderbolt_short)])
        if self.screamer_gender == "man" then
            ent:EmitSound(man_short_scream[math.random(1, #man_short_scream)])
        else
            ent:EmitSound(woman_short_scream[math.random(1, #woman_short_scream)])
        end
    end
    
    -- Add shock effect.
    local eData = EffectData();
    eData:SetEntity(ent);
    eData:SetOrigin(ent:GetPos());
    eData:SetStart(ent:GetPos());
    eData:SetScale(1);
    eData:SetMagnitude(15);
    util.Effect("TeslaHitBoxes", eData);

    self.Weapon:SetNextPrimaryFire(CurTime() + 5)
    
    timer.Simple(2, function() util.Effect("TeslaHitBoxes", eData); end)
    timer.Simple(4, function() util.Effect("TeslaHitBoxes", eData); end)
end

function SWEP:SecondaryAttack()
    if not IsFirstTimePredicted() then return end -- If it not checking first time predicted, it will be done many times. 
    local vstart = self.Owner:EyePos() + Vector(0, 0, -10)
    local trace = util.TraceLine({start=vstart, endpos = vstart + self.Owner:GetAimVector() * 75, filter = self.Owner})
    
    ent = trace.Entity
    if not (ent:IsPlayer() or ent:IsNPC()) then return end

    self.Owner:SetAnimation(PLAYER_ATTACK1)
    self.Weapon:SendWeaponAnim(ACT_VM_MISSCENTER)
    bullet = {}
    bullet.Src = self.Owner:GetShootPos()
    bullet.Dir = self.Owner:GetAimVector()
    bullet.Damage = 20
    self.Owner:FireBullets(bullet)
    
    -- Emit random sound for effect and scream.
    if SERVER then
        ent:EmitSound(thunderbolt_long[math.random(1, #thunderbolt_long)])
        if self.screamer_gender == "man" then
            ent:EmitSound(man_long_scream[math.random(1, #man_long_scream)])
        else
            ent:EmitSound(woman_long_scream[math.random(1, #woman_long_scream)])
        end
    end

    -- Add shock effect.
    local eData = EffectData();
    eData:SetEntity(ent);
    eData:SetOrigin(ent:GetPos());
    eData:SetStart(ent:GetPos());
    eData:SetScale(1);
    eData:SetMagnitude(15);
    util.Effect("TeslaHitBoxes", eData);

    self.Weapon:SetNextSecondaryFire(CurTime() + 15)
    
    timer.Simple(2, function() util.Effect("TeslaHitBoxes", eData); end)
    timer.Simple(4, function() util.Effect("TeslaHitBoxes", eData); end)
    timer.Simple(6, function() util.Effect("TeslaHitBoxes", eData); end)
    timer.Simple(8, function() util.Effect("TeslaHitBoxes", eData); end)
    timer.Simple(10, function() util.Effect("TeslaHitBoxes", eData); end)
    timer.Simple(12, function() util.Effect("TeslaHitBoxes", eData); end)
    timer.Simple(14, function() util.Effect("TeslaHitBoxes", eData); end)
    timer.Simple(15, function() util.Effect("TeslaHitBoxes", eData); end)
end

function SWEP:Reload()
    if (game.SinglePlayer()) then self:CallOnClient("Reload") end -- Force the client to read Reload function in singleplayer.
    if not IsFirstTimePredicted() then return end -- If it not checking first time predicted, it will be done many times.
    if CurTime() < self.last_reload_time + self.next_reload_time then return end
    
    self.last_reload_time = CurTime()
    if CLIENT then
        if self.screamer_gender == "man" then
            self.screamer_gender = "woman"
            LocalPlayer():ChatPrint("The screamer is now a woman")
        else
            self.screamer_gender = "man"
            LocalPlayer():ChatPrint("The screamer is now a man")
        end
        net.Start("update_vendemiaire_thunderbolt_config")
        net.WriteBool(self.screamer_gender)
        net.SendToServer()
    end
end

-- Change the screamer_gender var in the server side.
if SERVER then
    local function update_vendemiaire_thunderbolt_config(ln, ply)
        ply_thunderbolt = ply:GetWeapon("thunder_bolt")
        if ply_thunderbolt:IsValid() then
            ply_thunderbolt.screamer_gender = net.ReadBool()
        end
    end
    
    net.Receive("update_vendemiaire_thunderbolt_config", update_vendemiaire_thunderbolt_config)
end 
